#include <iostream>
#include <cmath>

class Zesp;

Zesp dodaj(const Zesp&, const Zesp&);

class Zesp
{
	public:

	using value_type = double;
	
	Zesp()
		: Zesp(0.f, 0.f)
	{}

	explicit Zesp(double r, double i = 0.f)
		: re(r)
		, im(i)
	{}

	template<typename result_value = value_type>
	double modul(){
		result_value wynik;
		wynik = sqrt(re*re + im*im);
		return wynik;
	}

	Zesp operator=(const Zesp& a)
	{
        this->re = a.re;
        this->im = a.im;
		return *this;
    }

    Zesp(const Zesp& z)
    	: Zesp(z.re, z.im)
    {}

    Zesp(Zesp&& z)
    	: Zesp(z.re, z.im)
    {
    	z.re = 0.f;
    	z.im = 0.f;
    }

    Zesp operator=(Zesp&& a)
    {
        this->re = a.re;
        this->im = a.im;
        a.re = 0.f;
        a.im = 0.f;
		return *this;
    }

    Zesp& dodaj(const Zesp& c) 
	{
		re=c.re+re;
		im=c.im+im;
		return *this;
	}

	Zesp dodaj(const Zesp& c) const
	{
		Zesp wynik;
		wynik.re=c.re+re;
		wynik.im=c.im+im;
		return wynik;
	}

	friend Zesp operator+(const Zesp& a, const Zesp& b);
	friend Zesp operator-(const Zesp& a, const Zesp& b);
	friend Zesp operator*(const Zesp& a, const Zesp& b);
	friend Zesp operator/(const Zesp& a, const Zesp& b);
	friend Zesp operator!(const Zesp& a);

	void pokaz() const
	{
		std::cout<< "liczba zespolona = "<<re<<" + "<<im<<"i "<< std::endl;
	}

private:
	value_type re,im;
};

Zesp dodaj(const Zesp& c1, const Zesp& c2)
{
	return c1.dodaj(c2);
}

Zesp operator+(const Zesp& a, const Zesp& b)
{
	return dodaj(a, b);
}

Zesp operator-(const Zesp& a, const Zesp& b)
{
	Zesp wynik;
	wynik.re=a.re-b.re;
	wynik.im=a.im-b.im;
	return wynik;
}

Zesp operator*(const Zesp& a, const Zesp& b)
{
	Zesp wynik;
	wynik.re=a.re*b.im+a.im*b.re;
	wynik.im=a.re*b.re-a.im*b.im;
	return wynik;
}

Zesp operator/(const Zesp& a, const Zesp& b)
{
	Zesp wynik;
	wynik.re=(a.re*b.re+a.im*b.im)/(b.re*b.re+b.im*b.im);
	wynik.im=(a.re*(-b.im)+a.im*b.re)/(b.re*b.re+b.im*b.im);
	return wynik;
}

Zesp operator!(const Zesp& a)
{
	Zesp wynik;
	wynik.re=a.re;
	wynik.im=-a.im;
	return wynik;
}

int main(){
	Zesp z1;
	z1.pokaz();

	Zesp z2(2,3);
	z2.pokaz(); 

	Zesp z3(5);
	z3.pokaz();

	Zesp z4(4,5);
	std::cout << "modul liczby zepolonej = " << z4.modul() << std::endl;

	Zesp z5(3,5);
	Zesp z6(2,3);
	Zesp z7=dodaj(z5,z6);
	z7.pokaz();

	Zesp z8(3,6);
	Zesp z9(2,3);
	Zesp z10=z9.dodaj(z8);
	z10.pokaz();

	Zesp z11(5,8);
	Zesp z12(1,2);
	Zesp z13 = z11 + z12;
	z13.pokaz();

	Zesp z14(5,8);
	Zesp z15(1,2);
	Zesp z16 = z14 - z15;
	z16.pokaz();

	Zesp z17(3,4);
	Zesp z18(5,2);
	Zesp z19 = z17 * z18;
	z19.pokaz();

	Zesp z20(3,2);
	Zesp z21(4,3);
	Zesp z22 = z20 / z21;
	z22.pokaz();

	Zesp z23(5,2);
	Zesp z24 = !z23;
	z24.pokaz();

	Zesp z25(3,2);
	Zesp z26(5,2);
	z26 = z25;
	z26.pokaz();

	Zesp z27(3,2);
	Zesp z28(5,2);
	z28 = std::move(z27);
	z27.pokaz();
	z28.pokaz();

	return 0;
}